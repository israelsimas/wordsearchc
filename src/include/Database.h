/*
 * Database.h
 *
 *  Created on: 18 de mai de 2017
 *      Author: simas
 */

#ifndef INCLUDE_DATABASE_H_
#define INCLUDE_DATABASE_H_

#include <iostream>

#include <iostream>
#include <string>
//#include <tr1/unordered_map>
#include <unordered_map>
#include <WordSearch.h>
#include <fstream>
#include <vector>
#include <errno.h>
#include <algorithm>
#include <dirent.h>
#include <list>
#include <string.h>

bool openDataBase(string pchDatabase);
bool sendQuery(const char *pchQuery, char *pchResult, int(*callback)(void*, int, char **, char **));
int convertTable(void *NotUsed, int argc, char **argv, char **azColName);


#endif /* INCLUDE_DATABASE_H_ */
