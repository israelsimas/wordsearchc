/*
 * WordSearch.h
 *
 *  Created on: 17 de mai de 2017
 *      Author: simas
 */

#ifndef INCLUDE_WORDSEARCH_H_
#define INCLUDE_WORDSEARCH_H_

#include <iostream>
#include <string>
//#include <tr1/unordered_map>
#include <unordered_map>
#include <WordSearch.h>
#include <fstream>
#include <vector>
#include <errno.h>
#include <algorithm>
#include <dirent.h>
#include <list>

using namespace std;

//#define PATH_BOOKS 				"/home/simas/Studies/workspace/WordsSearchC/Books/"
#define PATH_BOOKS 					"/Users/simas/Documents/Studies/BigData/workspace/WordsSearchC/Books/"
//#define PATH_BOOKS 	  		"/Users/simas/git/Words/books/books/"
//#define PATH_BOOKS 	  		"/home/simas/Desktop/books/books/books/"
#define MAX_LINE_LEN  			1000
#define MAX_WORD_LEN  			45
#define SPACE_CHAR	  			' '
#define INIT_SEARCH	 				"<<"
#define FINAL_SEARCH	 			">>"

#define START_MSG_CONSOLE		"Starting console"
#define START_MSG_SEACRH   	"Starting search"
#define ERROR_MSG_CONSOLE 	"Invalid Word"
#define ERROR2_MSG_CONSOLE  "Does not exist Word"
#define NUM_WORDS_SEARCH 		1

#define NUM_TRY_CMD_DATABASE 5

//#define DATABASE_PATH				"/Users/simas/Documents/Studies/BigData/workspace/WordsSearchC/database.sql"
#define DATABASE_PATH				"/data/database.sql"
#define CREATE_TABLE 				"CREATE TABLE TAB_CONTENT (PK INTEGER PRIMARY KEY  NOT NULL, IndexFile INTEGER, NumLine INTEGER, Line VARCHAR(1000))"
#define LENGTH_PARCIAL_RESULT  		1000
#define LENGHT_MAX_CONSULT 				2000

#define BUFFER_SELECT							200

#define WITH_THREADS							1
#define NUM_MAX_THREADS						10

#define IS_VALID_WORD(word)  (word.length() > 0) && (word.length() <= MAX_WORD_LEN)

/**
 * 	@struct WORD_INDEX
 *  Struct to store association between WORD and numLine.
 */
typedef struct WORD_INDEX {
	int indexFile;
	int line;
} WORD_INDEX;

void start();
std::string normalize(string s);
int getdir(string dir, vector<string> &files);
void insertWordsMap(string fileName, int indexFile);
string filterWord(string word);
void searchWord(string word);
string search(int indexFile, int numLine);
string filterSearch(string line, string word);

#endif /* INCLUDE_WORDSEARCH_H_ */
