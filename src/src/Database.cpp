/*
 * Database.cpp
 *
 *  Created on: 18 de mai de 2017
 *      Author: simas
 */

#include <sqlite3.h>
#include <Database.h>
#include <string>
#include <pthread.h>
#include <iostream>
#include <iostream>
#include <string>
//#include <tr1/unordered_map>
#include <unordered_map>
#include <WordSearch.h>
#include <fstream>
#include <vector>
#include <errno.h>
#include <algorithm>
#include <dirent.h>
#include <list>
#include <string.h>

pthread_mutex_t queryMutex 	 = PTHREAD_MUTEX_INITIALIZER;

char *pchDataBaseResult = NULL; 				// Ponteiro para os resultados de uma consulta ao banco de dados
int numStructTables 		= 0; 						// Resultados de uma contagem por linhas de uma tabela do banco de dados
sqlite3 *pDataBase 			= NULL;					// Ponteiro para acesso ao banco de dados
bool bDBopened 					= false;
bool bMutexQueryInitialized  = false;

bool openDataBase(string pchDatabase) {

	if (bDBopened) { 																										// Garante que está fechado antes de abrir.
		sqlite3_close(pDataBase);																					// Fecha o banco de dados
		bDBopened = false;																								// Sinaliza como fechado o banco de dados
	}

	pthread_mutex_init(&queryMutex, NULL);
	sqlite3_initialize();																								// Inicializa o SQLITE no sistema

	if (sqlite3_open(pchDatabase.c_str(), &pDataBase) == SQLITE_OK) {               // Não foi possível se conectar com  banco de dados?
		/*Passando BD para nosync: Now I'm going to turn synchronization off (with SQL command "PRAGMA synchronous = off") for sqlite engine.
		Which means do not synch with filesystem right away. This should increase the performance dramatically but more fragile during power/OS failures.
		Keep in mind that, this gives an unfair advantage to SQLite.*/

		sqlite3_config(SQLITE_CONFIG_MULTITHREAD);
		sqlite3_config(SQLITE_CONFIG_MEMSTATUS, 0);

		sendQuery("PRAGMA synchronous = off", NULL, NULL);
		sendQuery("PRAGMA temp_store = 2", NULL, NULL); 													// Tabelas temporárias e índices salvos em memória (2|MEMORY)
		sendQuery("PRAGMA secure_delete = off", NULL, NULL); 											// Quando secure_delete=on o que for apagado é substituído por zeros. Isso aumenta a segurança em detrimento da performance
		sendQuery("PRAGMA encoding = 'UTF-8'", NULL, NULL);

		bDBopened = true;			// Banco de Dados foi conectado com sucesso
	} else {

		sqlite3_close(pDataBase);
		pDataBase = NULL;

		bDBopened = false;		// Falha ao conectar com o Banco
	}

	return bDBopened;
}

bool sendQuery(const char *pchQuery, char *pchResult, int(*callback)(void*, int, char **, char **)) {

	bool statusConfig = false;
	int bStatusDB;
	char *pchMsgErr = NULL;
	int i;

	if (pchResult) {
		*pchResult 				= 0x0;
		pchDataBaseResult = pchResult;
	}

	// Tenta-se enviar o comando algumas vezes caso haja falha no envio. Isto ocorre devido a concorrência no banco de dados
	for (i = 0; i < NUM_TRY_CMD_DATABASE; i++) {
		bStatusDB = sqlite3_exec(pDataBase, pchQuery, callback, 0, &pchMsgErr);

		if (bStatusDB == SQLITE_OK) {
			statusConfig = true;
			break;
		} else if ((bStatusDB == SQLITE_BUSY) || (bStatusDB == SQLITE_LOCKED)) {
//			LOG("Busy [enviarQuery] Falhou (%s). Erro: (%s)",pchQuery, pchMsgErr);
//			msleep(TIME_WAIT_CMD_DATABASE);
		} else {
//			LOG_ERROR("Erro DB Status: %d, [enviarQuery] Falhou (%s). Erro: (%s)", bStatusDB, pchQuery, pchMsgErr);
			cout << "## CMD BANCO FALHOU: " << pchQuery << endl;
			break;
		}
	}

	if (pchMsgErr != NULL) {
		sqlite3_free(pchMsgErr);
	}

	return statusConfig;
}

int convertTable(void *NotUsed, int argc, char **argv, char **azColName) {

	int i;
	char pchParcialResult[LENGTH_PARCIAL_RESULT] = {'\0'};

	for (i = 0; i < argc; i++) {  													// Percorre o conteúdo de uma linha, cada parâmetro/coluna
		sprintf(pchParcialResult, "%s%c", argv[i] ? argv[i] : "NULL", ' ');
		strcat(pchDataBaseResult, pchParcialResult);
	}

	return 0;
}

