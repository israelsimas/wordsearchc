//============================================================================
// Name        : WordsSearchC.cpp
// Author      : Simas
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <WordSearch.h>
using namespace std;

#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <regex>
#include <Database.h>
#include <pthread.h>
#include <string.h>
#include <thread>
#include <unistd.h>

int indexFile = 0;
int numFiles = 0;
//tr1::unordered_map<string, std::list*> hashWords;
unordered_map<string, std::list<WORD_INDEX *> *> hashWords;
unordered_map<int, string> hashFileIndex;
std::vector<std::thread> workers;

pthread_mutex_t queryHash = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t queryHashFile = PTHREAD_MUTEX_INITIALIZER;
char pchResultDb[LENGHT_MAX_CONSULT];

template<typename Out>
void split(const std::string &s, char delim, Out result) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		*(result++) = item;
	}
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, std::back_inserter(elems));
	return elems;
}

int main() {

	start();

	return 0;
}

static inline std::string &ltrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
  return s;
}



void startConsole() {

	cout << START_MSG_CONSOLE << endl;

	while (true) {
		string word;

		cout << "\n\n Type a word: " << endl;
		cin >> word;

		// Filter the word
		string filteredWord = filterWord(word);
		cout << "## filteredWord: " << filteredWord << "#" << endl;

		if (IS_VALID_WORD(filteredWord)) {

			if (hashWords.find(filteredWord) != hashWords.end()) {
				searchWord(filteredWord);
			} else {
				cout << ERROR2_MSG_CONSOLE << endl;
			}

		} else {
			cout << ERROR_MSG_CONSOLE << endl;
		}

	}
}

void start() {

	openDataBase(DATABASE_PATH);

	pthread_mutex_init(&queryHash, NULL);
	pthread_mutex_init(&queryHashFile, NULL);

	string dir = string(PATH_BOOKS);
	vector<string> files = vector<string>();

	getdir(dir, files);

	numFiles = files.size();

#ifdef WITH_THREADS
	int indexFileName = 0;

	while (indexFileName < numFiles) {

		for (int i = 0; i < NUM_MAX_THREADS; ++i) {

			if ((indexFileName < numFiles) && (files[indexFileName].length() > 2)) {
				workers.push_back(std::thread(insertWordsMap, files[indexFileName], ++indexFile));
			}

			indexFileName++;
		}

		for (std::thread &t : workers) {
			if (t.joinable()) {
				t.join();
			}
		}
	}

#else

	for (unsigned int i = 0; i < files.size(); i++) {

		if (files[i].length() > 2) {
			insertWordsMap(files[i], ++indexFile);
		}
	}
#endif

	startConsole();
}

void insertWordsMap(string fileName, int indexFile) {

	cout << "File " << indexFile << "/" << numFiles << " in process, name: " << fileName << endl;

	string path = PATH_BOOKS + fileName;
	std::ifstream input(path.c_str());
	int numLine = 1;

	pthread_mutex_lock(&queryHashFile);
	hashFileIndex[indexFile] = fileName;
	pthread_mutex_unlock(&queryHashFile);

	for (std::string line; getline(input, line);) {

		if (line.length() > MAX_LINE_LEN) {
			numLine++;
			continue;
		}

		std::regex e ("[^a-zA-Z ]");
		string newLine = normalize(line);
		newLine = std::regex_replace(newLine, e, " ");

		std::vector<std::string> splitSrt = split(newLine.c_str(), SPACE_CHAR);
		unordered_map<string, int> hashWordsLine;

		for (unsigned int j = 0; j < splitSrt.size(); j++) {

			if (IS_VALID_WORD(splitSrt[j])) {

				pthread_mutex_lock(&queryHash);
				if (hashWords.find(splitSrt[j]) != hashWords.end()) {

					if (hashWordsLine.find(splitSrt[j]) == hashWordsLine.end()) {
					  WORD_INDEX *wordIndex = new WORD_INDEX();
					  wordIndex->indexFile = indexFile;
					  wordIndex->line 		 = numLine;
					  std::list<WORD_INDEX *> *listWordIndex = hashWords[splitSrt[j]];
					  listWordIndex->push_back(wordIndex);
						hashWordsLine[splitSrt[j]] = 0;
					}

				} else {
				  std::list<WORD_INDEX *> *listWordIndex = new list<WORD_INDEX *>;
				  WORD_INDEX *wordIndex = new WORD_INDEX();
				  wordIndex->indexFile = indexFile;
				  wordIndex->line 		 = numLine;
				  listWordIndex->push_back(wordIndex);
				  hashWords[splitSrt[j]] 		 = listWordIndex;
				  hashWordsLine[splitSrt[j]] = 0;

				}
				pthread_mutex_unlock(&queryHash);
			}
		}

		hashWordsLine.clear();

		numLine++;
	}

}

std::string normalize(string s) {

	std::transform(s.begin(), s.end(), s.begin(), ::tolower);

	return s;
}

int getdir(string dir, vector<string> &files) {
	DIR *dp;
	struct dirent *dirp;

	if ((dp = opendir(dir.c_str())) == NULL) {
		cout << "Error(" << errno << ") opening " << dir << endl;
		return errno;
	}

	while ((dirp = readdir(dp)) != NULL) {
		files.push_back(string(dirp->d_name));
	}
	closedir(dp);

	return 0;
}


/*
 * Search in database a specific word.
 */
void searchWord(string word) {

	cout << "########## Showing the results ##########" << endl;

	std::list<WORD_INDEX *> *listWordIndex = hashWords[word];

	for (std::list<WORD_INDEX *>::iterator it = listWordIndex->begin(); it != listWordIndex->end(); it++) {
		std::cout << "File: " << hashFileIndex[(*it)->indexFile] << ", Line: " << (*it)->line << endl;
		 cout << filterSearch(search((*it)->indexFile, (*it)->line), word) << endl;
	}

	cout << "########################################" << endl;
}

/*
 * Filter word removing special characters and space
 */
string filterWord(string word) {

	std::regex e ("[^a-zA-Z ]");

	string newWord = ltrim(word);;
	newWord = normalize(std::regex_replace(newWord, e, " "));

	return newWord;
}

/*
 * Search in database a specific line content for a specific File.
 */
string search(int indexFile, int numLine) {

	// Insert the code to search in database. So, will be a "SELECT line FROM TAB_CONTENT WHERE indexFile='IndexFile' and numLine='numLine';
	string sql = "SELECT Line FROM TAB_CONTENT WHERE IndexFile=? and NumLine=?";
	char pchQuery[BUFFER_SELECT];

	sprintf(pchQuery,  "SELECT line FROM TAB_CONTENT WHERE indexFile=%d and numLine=%d;", indexFile, numLine);
	sendQuery(pchQuery, pchResultDb, convertTable);

	return pchResultDb;
}

/*
 * Filter the line from Database, so replace/substitute "Word" for "<< Word >>".
 * For this, filter the word to lower case, then substitute for the new format.
 */
string filterSearch(string line, string word) {

	string replace = INIT_SEARCH + word + FINAL_SEARCH;
  size_t pos     = 0;

	while ((pos = line.find(word, pos)) != std::string::npos) {
		line.replace(pos, word.length(), replace);
		pos += replace.length();
	}

	return line;
}

